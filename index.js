/*var http = require("http");*/
const express = require('express');
const request = require('request');
const http = require('http');
const PORT = process.env.PORT || 8080;
var app = express();

app.get('/dog', function(req, res) {
    request('https://dog.ceo/api/breeds/image/random', {json: true}, (err, res2, body) => {
        if (err) { return console.log(err); }
        console.log(body.status);
        console.log(body.message);
        res.send('<img src="' + body.message + '">\n');
    });
})

app.get('/fox', function(req, res) {
    request('https://randomfox.ca/floof', {json: true}, (err, res2, body) => {
        if (err) { return console.log(err); }
        console.log(body.image);
        res.send('<img src="' + body.image + '">\n');
    });
})

app.get('/hello', function(req, res) {
    res.send('<b>Hello World</b>');
})

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/" + "index.html");
})

app.get('/datenschutz', function (req, res) {
    res.sendFile(__dirname + "/" + "datenschutz.html");
})

var server = app.listen(PORT, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
})

/* Hello, World! program in node.js */
console.log("Hello, World!")

